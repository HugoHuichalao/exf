package com.mycompany.dictionary;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author Hugo
 */
@ApplicationPath("api")
public class AppConfig extends Application {
    
}
