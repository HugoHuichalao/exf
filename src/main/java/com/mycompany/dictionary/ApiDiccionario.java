/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dictionary;

import com.dao.controller.PalabrasJpaController;
import com.controller.entity.Palabras;
import com.dto.PalabraDTO;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.json.simple.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Hugo
 */
@Path("diccionario")
public class ApiDiccionario {
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idbuscar") String idbuscar){
    
        try {
            Client client = ClientBuilder.newClient();
            
            WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/" + idbuscar);
            PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("app_id", "384b3eda").header("app_key", "14c1735e7d068e5e9ac10c78cc85a197").get(PalabraDTO.class);
            
            Palabras pal = new Palabras();
            
            pal.setPalabra(palabradto.getWord());
            Date fecha = new Date();
            pal.setFecha(fecha.toString());
            Random rand = new Random();
            int upperbound = 1000;
        
            int intRandom = rand.nextInt(upperbound); 
        
            String s = String.valueOf(intRandom);
            pal.setIdPalabra(s);
            
            String definicion = (String) palabradto.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
            pal.setDefinicion(definicion);
            
            PalabrasJpaController dao = new PalabrasJpaController();
            
            dao.create(pal);
            
            return Response.ok(200).entity(pal).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiDiccionario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
    
   @GET
    @Path("/historial")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarClientes(){
        
     PalabrasJpaController dao = new PalabrasJpaController();
     
     List<Palabras> lista = dao.findPalabrasEntities();
     
     return Response.ok(200).entity(lista).build();
    }
}
