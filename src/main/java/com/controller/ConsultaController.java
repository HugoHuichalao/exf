/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.dao.controller.PalabrasJpaController;
import com.controller.entity.Palabras;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Hugo
 */
@WebServlet(name = "ConsultaController", urlPatterns = {"/ConsultaController"})
public class ConsultaController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConsultaController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConsultaController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        String idbuscar = request.getParameter("idbuscar");
        
        if(accion.equals("buscar")) {
            Client client = ClientBuilder.newClient();
              WebTarget myResource = client.target("https://app-diccionario.herokuapp.com/api/diccionario/" + idbuscar);

              Palabras definicion = (Palabras) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<Palabras>() {
            });
              
                try {
                String id_palabra = request.getParameter("id_palabra");
                String palabra = request.getParameter("palabra");
                String def = request.getParameter("definicion");
                String fecha = request.getParameter("fecha");
                
                Palabras palabras = new Palabras();
                palabras.setIdPalabra(id_palabra);
                palabras.setPalabra(palabra);
                palabras.setDefinicion(def);
                palabras.setFecha(fecha);
                
                
                PalabrasJpaController dao = new PalabrasJpaController();
                
                dao.create(palabras);
                 
               
            } catch (Exception ex) {
                Logger.getLogger(ConsultaController.class.getName()).log(Level.SEVERE, null, ex);
            }
    
            
          
               request.setAttribute("definicion", definicion);
               request.getRequestDispatcher("definicion.jsp").forward(request, response);
        }
        
       if (accion.equals("listar")) {
             Client client = ClientBuilder.newClient();
              WebTarget myResource = client.target("https://app-diccionario.herokuapp.com/api/diccionario/historial");

              List<Palabras> lista = (List<Palabras>) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Palabras>>() {
            });
             
          
               request.setAttribute("listaPalabras", lista);
               request.getRequestDispatcher("historial.jsp").forward(request, response);
            
        }
       
        if (accion.equals("volver")) {
            
            try {
                String id_palabra = request.getParameter("id_palabra");
                String palabra = request.getParameter("palabra");
                String definicion = request.getParameter("definicion");
                String fecha = request.getParameter("fecha");
                
                Palabras palabras = new Palabras();
                palabras.setIdPalabra(id_palabra);
                palabras.setPalabra(palabra);
                palabras.setDefinicion(definicion);
                palabras.setFecha(fecha);
                
                
                PalabrasJpaController dao = new PalabrasJpaController();
                
                dao.create(palabras);
                 
               
            } catch (Exception ex) {
                Logger.getLogger(ConsultaController.class.getName()).log(Level.SEVERE, null, ex);
            }
             request.getRequestDispatcher("index.jsp").forward(request, response);
            
        } 
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
