/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

import java.util.List;

/**
 *
 * @author Hugo
 */
public class Sens{
    public List<String> definitions;
    public String id;
    public List<Note> notes;
    
    public List<String> getDefinitions() {
        return this.definitions;
    } 
    
    public void setDefinitions(List<String> definitions) {
        this.definitions = definitions;
    } 
}
