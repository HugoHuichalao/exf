

<%@page import="com.controller.entity.Palabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Palabras definicion = (Palabras) request.getAttribute("definicion");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definición</title>
    </head>
    <body>
        
        <form  name="form" action="ConsultaController" method="POST">
            <h1><%= definicion.getPalabra().toString() %></h1>
            <h2><%= definicion.getDefinicion().toString() %></h2> 
            <h2>Fuente: Oxford Dictionaries</h2>
            <button type="submit" name="accion" value="volver" class="btn btn-success">REGRESAR</button>
         </form>
    </body>
</html>
